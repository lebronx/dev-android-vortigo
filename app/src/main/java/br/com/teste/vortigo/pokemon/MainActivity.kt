package br.com.teste.vortigo.pokemon

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import br.com.teste.vortigo.pokemon.ui.finder.PokemonFragment
import br.com.teste.vortigo.pokemon.ui.home.HomeFragment
import br.com.teste.vortigo.pokemon.ui.register.RegisterFragment

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
}