package br.com.teste.vortigo.pokemon

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import br.com.teste.vortigo.pokemon.ui.register.RegisterFragment
import br.com.teste.vortigo.pokemon.ui.splash.SplashViewModel

class SplashFragment : Fragment() {

    private var splashViewModel: SplashViewModel? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.splash_activity, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val progressBar = view?.findViewById<ProgressBar>(R.id.progressBar)
        val btnNext = view?.findViewById<Button>(R.id.btn_letsGo)

        progressBar.visibility = View.GONE
        btnNext?.setOnClickListener {
            progressBar.visibility = View.VISIBLE

            findNavController().navigate(R.id.registerFragment)
            progressBar.visibility = View.GONE
        }

    }
}