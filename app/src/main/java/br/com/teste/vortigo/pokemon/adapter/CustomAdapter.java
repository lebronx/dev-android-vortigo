package br.com.teste.vortigo.pokemon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.teste.vortigo.pokemon.R;
import br.com.teste.vortigo.pokemon.model.PokeType;

public class CustomAdapter extends ArrayAdapter<PokeType> {

    private ArrayList<PokeType> arrayList;


    public CustomAdapter(@NonNull Context context, int resource, @NonNull ArrayList<PokeType> pokeTypes) {
        super(context, resource, pokeTypes);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable
            View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable
            View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_details, parent, false);
        }

        TextView textViewName = convertView.findViewById(R.id.txtDescription);
        ImageView imageView = convertView.findViewById(R.id.imgDetail);
        arrayList = getItem(position).getResults();

        if (arrayList != null) {
            textViewName.setText(arrayList.get(position).getName());
            Glide.with(convertView)
                    .load(arrayList.get(position).getThumbnailImage())
                    .placeholder(android.R.color.transparent)
                    .into(imageView);
        }
        return convertView;
    }
}
