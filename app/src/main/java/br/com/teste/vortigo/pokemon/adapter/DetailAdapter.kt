package br.com.teste.vortigo.pokemon.adapter

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SpinnerAdapter
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import br.com.teste.vortigo.pokemon.R
import br.com.teste.vortigo.pokemon.model.PokeType
import br.com.teste.vortigo.pokemon.utils.PokemonColorUtil
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_details.view.*
import kotlinx.android.synthetic.main.item_pokemon.view.*

/*
class DetailAdapter(private val list: List<PokeType>, private val context: Context) : RecyclerView.Adapter<DetailAdapter.ViewHolder>(){
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(item: PokeType) {
            val listCollor = item.results.toString()
            itemView.txtDescription.text = item.results[0].name

            val color = PokemonColorUtil(itemView.context).getPokemonColor(listOf(listCollor))
            itemView.relativeLayoutBackground.background.colorFilter =
                PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP)

            item.results?.getOrNull(0).let { firstType ->
                itemView.txtDescription.text = firstType.toString()
                itemView.txtDescription.isVisible = firstType != null
            }

            Glide.with(itemView.context)
                .load(item.results[0].thumbnailImage)
                .placeholder(android.R.color.transparent)
                .into(itemView.imgDetail)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_details, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.bindView(item)
    }

    override fun getItemCount(): Int {
        return list.size
    }

}*/
