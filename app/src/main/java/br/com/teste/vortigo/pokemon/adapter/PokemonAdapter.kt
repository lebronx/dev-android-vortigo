package br.com.teste.vortigo.pokemon.adapter

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import br.com.teste.vortigo.pokemon.R
import br.com.teste.vortigo.pokemon.model.Pokemon
import br.com.teste.vortigo.pokemon.utils.PokemonColorUtil
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_pokemon.view.*
import kotlinx.android.synthetic.main.splash_activity.view.*

class PokemonAdapter (private val list: List<Pokemon>, private val context: Context) : RecyclerView.Adapter<PokemonAdapter.ViewHolder>(){
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(item: Pokemon) {
            itemView.txtNamePoke.text = item.name

            val color = PokemonColorUtil(itemView.context).getPokemonColor(item.type)
            itemView.relativeLayoutBackground.background.colorFilter =
                PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP)

            item.type?.getOrNull(0).let { firstType ->
                itemView.txtNamePoke.text = firstType
                itemView.txtNamePoke.isVisible = firstType != null
            }

            Glide.with(itemView.context)
                .load(item.thumbnailImage)
                .placeholder(android.R.color.transparent)
                .into(itemView.imgPokemon)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_pokemon, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.bindView(item)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}