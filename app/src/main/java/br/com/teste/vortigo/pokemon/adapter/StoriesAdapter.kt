package br.com.teste.vortigo.pokemon.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.teste.vortigo.pokemon.R
import br.com.teste.vortigo.pokemon.model.PokeType
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_pokemon.view.*
import kotlinx.android.synthetic.main.item_stories.view.*


class StoriesAdapter(private val list: List<PokeType>, private val context: Context) : RecyclerView.Adapter<StoriesAdapter.ViewHolder>(){
    

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(item: PokeType) {
            itemView.textName.text = item.results[position].name

            Glide.with(itemView.context)
                .load(item.thumbnailImage)
                .placeholder(android.R.color.transparent)
                .into(itemView.imageStories)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_stories, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.bindView(item)
    }

    override fun getItemCount(): Int {
        return list.size
    }



}