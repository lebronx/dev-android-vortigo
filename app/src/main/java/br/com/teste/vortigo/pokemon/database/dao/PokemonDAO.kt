package br.com.teste.vortigo.pokemon.database.dao

import android.database.Observable
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import br.com.teste.vortigo.pokemon.model.People
import br.com.teste.vortigo.pokemon.model.PokeType
import br.com.teste.vortigo.pokemon.model.Pokemon

@Dao
interface PokemonDAO {

    /*@Query("SELECT * FROM pokemon WHERE id = :id")
    fun getById(id: String?): LiveData<Pokemon>

    @Query("SELECT * FROM pokemon WHERE id IN(:evolutionIds)")
    fun getEvolutionsByIds(evolutionIds: List<String>): LiveData<List<Pokemon>>

    @Query("SELECT * FROM pokemon")
    fun all(): LiveData<List<Pokemon>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(pokemon: List<Pokemon>)

    @Query("DELETE FROM pokemon")
    fun deleteAll()

    @Delete
    fun delete(model: Pokemon)

    @Query("SELECT * FROM pokeType")
    fun allTypes(): LiveData<List<PokeType>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addTypes(pokemon: List<PokeType>)
*/
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addTrainer(people: String)
}
