package br.com.teste.vortigo.pokemon.di

val appComponent = listOf(
    networkModule,
    viewModelsModule
)