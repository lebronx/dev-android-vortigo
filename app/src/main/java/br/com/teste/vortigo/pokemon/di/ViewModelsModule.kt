package br.com.teste.vortigo.pokemon.di

import br.com.teste.vortigo.pokemon.ui.finder.PokemonFinderViewModel
import br.com.teste.vortigo.pokemon.ui.home.HomeViewModel
//import br.com.teste.vortigo.pokemon.ui.register.RegisterViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelsModule = module {
    //viewModel { RegisterViewModel(get()) }
    viewModel { PokemonFinderViewModel(get()) }
    viewModel { HomeViewModel(get()) }
}