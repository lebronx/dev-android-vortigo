package br.com.teste.vortigo.pokemon.model

import androidx.room.Entity


@Entity
data class People(
    val id: Int,
    val namePeople: String,
    val pokeType: String
)