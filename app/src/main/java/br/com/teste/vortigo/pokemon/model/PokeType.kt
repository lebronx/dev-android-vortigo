package br.com.teste.vortigo.pokemon.model

import androidx.room.Entity
import com.google.gson.annotations.SerializedName


@Entity
data class PokeType(
    @SerializedName("results")
    val results: ArrayList<PokeType>,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("thumbnailImage")
    val thumbnailImage: String
)