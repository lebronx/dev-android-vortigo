package br.com.teste.vortigo.pokemon.model

import androidx.room.Entity
import androidx.room.TypeConverters
import br.com.teste.vortigo.pokemon.utils.ListStringConverter
import com.google.gson.annotations.SerializedName

@Entity
@TypeConverters(ListStringConverter::class)
data class Pokemon(
    @SerializedName("abilities")
    val abilities: List<String>,
    @SerializedName("collectibles_slug")
    val collectibles_slug: String,
    @SerializedName("detailPageURL")
    val detailPageURL: String,
    @SerializedName("featured")
    val featured: String,
    @SerializedName("height")
    val height: Double,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("number")
    val number: String,
    @SerializedName("slug")
    val slug: String,
    @SerializedName("thumbnailAltText")
    val thumbnailAltText: String,
    @SerializedName("thumbnailImage")
    val thumbnailImage: String,
    @SerializedName("type")
    val type: List<String>,
    @SerializedName("weakness")
    val weakness: List<String>,
    @SerializedName("weight")
    val weight: Double
)