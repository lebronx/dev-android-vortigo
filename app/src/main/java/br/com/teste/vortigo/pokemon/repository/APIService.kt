package br.com.teste.vortigo.pokemon.repository

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

object APIService {
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://vortigo.blob.core.windows.net/files/pokemon/data/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val pokemonService: PokemonService = retrofit.create()
}
