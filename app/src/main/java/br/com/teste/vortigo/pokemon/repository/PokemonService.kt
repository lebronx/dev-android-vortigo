package br.com.teste.vortigo.pokemon.repository


import android.database.Observable
import br.com.teste.vortigo.pokemon.model.PokeType
import br.com.teste.vortigo.pokemon.model.Pokemon
import retrofit2.Call
import retrofit2.http.GET

interface PokemonService {
    @GET("pokemons.json")
    fun getPokemons(): Call<MutableList<Pokemon>>

    @GET("types.json")
    fun getTypes(): Call<PokeType>

}
