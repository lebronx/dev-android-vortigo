package br.com.teste.vortigo.pokemon.ui.finder

import android.database.Observable
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.teste.vortigo.pokemon.model.PokeType
import br.com.teste.vortigo.pokemon.repository.PokemonService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.thread

class PokemonFinderViewModel (private val pokemonService: PokemonService) : ViewModel(){

    private val mutableLiveData = MutableLiveData<PokeType>()
    init {
        initNetworkRequest()
    }

    private fun initNetworkRequest() {
        val call = pokemonService.getTypes()
        call.enqueue(object : Callback<PokeType> {
            override fun onResponse(call: Call<PokeType>, response: Response<PokeType>) {
                response.body()?.let { pokemonsTypes: PokeType ->
                        val pokemonResponse = response.body()
                        pokemonResponse?.let { mutableLiveData.value = pokemonsTypes as PokeType }
                }
            }

            override fun onFailure(call: Call<PokeType>, t: Throwable?) {
                // TODO handle
                Log.e("error", t!!.localizedMessage)
            }
        })
    }

    fun getPokemonDetails(): MutableLiveData<PokeType> {
        return mutableLiveData
    }

}