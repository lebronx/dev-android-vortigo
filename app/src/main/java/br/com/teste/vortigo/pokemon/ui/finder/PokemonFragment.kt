package br.com.teste.vortigo.pokemon.ui.finder

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import br.com.teste.vortigo.pokemon.R
import br.com.teste.vortigo.pokemon.adapter.CustomAdapter
import br.com.teste.vortigo.pokemon.model.PokeType
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.concurrent.thread


class PokemonFragment : Fragment() {

    private val pokemonFinderViewModel: PokemonFinderViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_poke_finder, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val progressBar = view.findViewById<ProgressBar>(R.id.progressBar)

        progressBar.visibility = View.GONE

        val imageButton = view.findViewById<ImageButton>(R.id.imageButton)
        val imageBack = view.findViewById<ImageButton>(R.id.imageBack)
        val spinner = view.findViewById<Spinner>(R.id.spinner)

        var detailsPokemons: ArrayList<PokeType>

        pokemonFinderViewModel.getPokemonDetails().observe(viewLifecycleOwner, Observer {
            /*detailsPokemons = arrayOf(it)*/
            val adapter = CustomAdapter(view.context, R.layout.item_stories, arrayListOf(it))

            adapter.addAll(it)
            adapter.setDropDownViewResource(R.layout.item_stories)
            spinner!!.adapter=adapter

            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
                ) {
                    val item = parent.getItemAtPosition(position)

                }

                override fun onNothingSelected(parent: AdapterView<*>) {

                }
            }

        })


        spinner?.onItemSelectedListener = object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val item = spinner.adapter.getItem(position)
            }

        }

        imageBack.setOnClickListener {
            thread {
                activity?.onBackPressed()

            }
        }

        imageButton.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            findNavController().navigate(R.id.homeFragment)
        }

    }

}