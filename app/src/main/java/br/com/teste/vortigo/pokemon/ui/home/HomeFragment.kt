package br.com.teste.vortigo.pokemon.ui.home

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.teste.vortigo.pokemon.R
import br.com.teste.vortigo.pokemon.adapter.PokemonAdapter
import br.com.teste.vortigo.pokemon.adapter.StoriesAdapter
import br.com.teste.vortigo.pokemon.model.PokeType
import br.com.teste.vortigo.pokemon.model.Pokemon
import br.com.teste.vortigo.pokemon.ui.finder.PokemonFinderViewModel
import br.com.teste.vortigo.pokemon.utils.PokemonColorUtil
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    private val homeViewModel: HomeViewModel by viewModel()
    private val pokemonFinderViewModel: PokemonFinderViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.window?.statusBarColor =
                PokemonColorUtil(view.context).convertColor(R.color.background)

        val progressBar = view.findViewById<ProgressBar>(R.id.progressBar)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        val recyclerViewStories = view.findViewById<RecyclerView>(R.id.recyclerViewStories)

        val layoutManager = GridLayoutManager(context, 1)
        recyclerView.layoutManager = layoutManager

        homeViewModel.getListPokemon().observe(viewLifecycleOwner, Observer {
            val pokemons: List<Pokemon> = it
            recyclerView.adapter = PokemonAdapter(pokemons, view.context)
            if (pokemons.isNotEmpty())
                progressBar.visibility = View.GONE
        })

        pokemonFinderViewModel.getPokemonDetails().observe(viewLifecycleOwner, Observer {
            var detailsPokemons = listOf(it)
            recyclerViewStories.adapter = StoriesAdapter(detailsPokemons, view.context)
        })

    }


}