package br.com.teste.vortigo.pokemon.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.teste.vortigo.pokemon.model.PokeType
import br.com.teste.vortigo.pokemon.model.Pokemon
import br.com.teste.vortigo.pokemon.repository.PokemonService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.concurrent.thread

class HomeViewModel(private val pokemonService: PokemonService) : ViewModel() {

    private val mutableLiveData = MutableLiveData<ArrayList<Pokemon>>()

    init {
        initNetworkRequest()
    }

    private fun initNetworkRequest() {
        val call = pokemonService.getPokemons()
        call.enqueue(object : Callback<MutableList<Pokemon>?> {
            override fun onResponse(call: Call<MutableList<Pokemon>?>, response: Response<MutableList<Pokemon>?>) {
                response.body()?.let { pokemons: MutableList<Pokemon> ->
                    val pokemonResponse = response.body()
                    pokemonResponse?.let { mutableLiveData.value = pokemons as ArrayList<Pokemon> }
                }
            }

            override fun onFailure(call: Call<MutableList<Pokemon>?>?, t: Throwable?) {
                // TODO handle failure
                Log.e("error", t!!.localizedMessage)
            }
        })
    }

    fun getListPokemon(): MutableLiveData<ArrayList<Pokemon>> {
        return mutableLiveData // pokemonDAO.all()
    }
}