package br.com.teste.vortigo.pokemon.ui.register

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import br.com.teste.vortigo.pokemon.R
import org.koin.androidx.viewmodel.ext.android.viewModel


class RegisterFragment : Fragment() {

    val registerViewModel: RegisterViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val progressBar = view.findViewById<ProgressBar>(R.id.progressBar)

        val sTrainer = view.findViewById<EditText>(R.id.edtName)
        sTrainer.setOnEditorActionListener { _, actionId, event ->
            if (event != null && event.keyCode === KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                Log.i(TAG, "Enter pressed")
            }
            false
        }

        val buttonNext = view.findViewById<ImageButton>(R.id.imgNext)
        buttonNext?.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            if (validaCampoNome(sTrainer!!.text.toString())){
                findNavController().navigate(R.id.pokemonFragment)
            } else {
                Toast.makeText(context, "Preencha o campo ou Nome pequeno!", Toast.LENGTH_SHORT).show()

            }
            progressBar.visibility = View.INVISIBLE
        }

    }


    fun validaCampoNome(sName: String): Boolean{
        if (sName.length >= 3 && sName.isNotEmpty()){
            return true
        }
        return false
    }


}