package br.com.teste.vortigo.pokemon.ui.register

import android.content.Context
import android.database.Observable
import android.view.View
import android.view.ViewManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.teste.vortigo.pokemon.database.dao.PokemonDAO
import br.com.teste.vortigo.pokemon.model.People
import br.com.teste.vortigo.pokemon.model.Pokemon
import br.com.teste.vortigo.pokemon.repository.PokemonService

class RegisterViewModel(private val pokemonDAO: PokemonDAO, private val context: Context) : ViewModel() {

    fun setTrainer(sName: String) {
        pokemonDAO.addTrainer(sName)
    }
}
